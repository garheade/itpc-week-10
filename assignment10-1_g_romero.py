# Assignment 10.1 ITP
# Developer: Gary Romero
# Date: May 19, 2022
#

"""
This week we will create a program that performs file processing activities. Your program this week will use the OS library in order to validate that a directory exists before creating a file in that directory. Your program will prompt the user for the directory they would like to save the file in as well as the name of the file. The program should then prompt the user for their name, address, and phone number. Your program will write this data to a comma separated line in a file and store the file in the directory specified by the user.
Once the data has been written your program should read the file you just wrote to the file system and display the file contents to the user for validation purposes.
Submit a link to your Github repository.
"""

# Imports
import os
import csv

#Variables
user_dir = ''
filename = ''
finalpath = ''
userinfo_dict = {}
fields = []
rows = []

#functions

def defineUserDir():
    # Ask user for directory to save info
    global finalpath
    user_dir = input("What directory would you like to save you information in? ")
    filename = input("What would you like to name the file? ")
    finalpath = user_dir+"/"+filename
    print(f"The directory and file you choose is {finalpath}.")
    y_n = input("Is this correct? (y/n)")
    if y_n == 'y':
        try:
            isFile = os.path.isfile(user_dir)
        except:
            print("This directory doesn't exist.")
    else:
        print("Please try again.")

    return finalpath


def requestUserInfo():
    # Ask user for personal info"""
    userinfo_dict['name'] = input("What is your name? ")
    userinfo_dict['streetadd'] = input("What is your street address?")
    userinfo_dict['city'] = input("What is your city?")
    userinfo_dict['state'] = input("What is your state?")
    userinfo_dict['zip'] = input("What is your zip?")
    userinfo_dict['phone'] = input("What is your phone number?")
    print("\nThank you. We'll now save your information.")


def saveUserInfo():
    #Take userinput and save to user_dir
#    fb = open(finalpath, 'x')
#    fb.close()

    with open(finalpath, 'w') as file_object:
        for key, value in userinfo_dict.items():
            file_object.write(value+',')
    print("\nYour information has been saved.\n")


def displayUserInfo():
    # Display user input data
    print("The information saved to the file is:")
    with open(finalpath, 'r') as csvfile:
        data = csv.reader(csvfile)
        fields = next(data)
        for row in data:
            rows.append(row)

    for field in fields:
        print(field)



# Main Program

print("Welcome to the file save example program.\n")
defineUserDir()
requestUserInfo()
saveUserInfo()
displayUserInfo()
print("Thank you for using our example!")
